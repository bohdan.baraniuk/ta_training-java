package com.epam.training.bohdan_baraniuk.task3.pages;

import com.epam.training.bohdan_baraniuk.task3.utils.WaitHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CloudCalculatorPage {
    private final WebDriver driver;

    @FindBy(xpath = "//span[text()='Add to estimate']")
    private WebElement addToEstimateButton;

    @FindBy(xpath = "//h2[text()='Compute Engine']")
    private WebElement computeEngineButton;

    @FindBy(id = "c11")
    private WebElement instancesNumberField;

    @FindBy(xpath = "//button[@aria-label='Add GPUs']")
    private WebElement addGpuButton;

    @FindBy(xpath = "//ul[@aria-label='Operating System / Software']/../..")
    private WebElement osDropDown;

    @FindBy(xpath = "//li[@data-value='free-debian-centos-coreos-ubuntu-or-byol-bring-your-own-license']")
    private WebElement osOption;

    @FindBy(xpath = "//label[text()='Regular']")
    private WebElement regularProvisioningModelButton;

    @FindBy(xpath = "//ul[@aria-label='Series']/../..")
    private WebElement seriesDropDown;

    @FindBy(xpath = "//li[@data-value='n1']")
    private WebElement seriesOption;

    @FindBy(xpath = "//ul[@aria-label='Machine type']/../..")
    private WebElement machineTypeDropDown;

    @FindBy(xpath = "//li[@data-value='n1-standard-8']")
    private WebElement machineTypeOption;

    @FindBy(xpath = "//ul[@aria-label='GPU Model']/../..")
    private WebElement gpuModelDropDown;

    @FindBy(xpath = "//li[@data-value='nvidia-tesla-v100']")
    private WebElement gpuModelOption;

    @FindBy(xpath = "//ul[@aria-label='Number of GPUs']/../..")
    private WebElement gpuNumberDropDown;

    @FindBy(xpath = "//li[@data-value='1']")
    private WebElement gpuNumberOption;

    @FindBy(xpath = "//ul[@aria-label='Local SSD']/../..")
    private WebElement localSsdDropDown;

    @FindBy(xpath = "//span[text()='2x375 GB']/../..")
    private WebElement localSsdOption;

    @FindBy(xpath = "//ul[@aria-label='Region']/../..")
    private WebElement regionDropDown;

    @FindBy(xpath = "//li[@data-value='europe-west4']")
    private WebElement regionOption;

    @FindBy(xpath = "//label[text()='1 year']")
    private WebElement committedUsageButton;

    @FindBy(xpath = "//button[@aria-label='Open Share Estimate dialog']")
    private WebElement shareEstimateButton;

    @FindBy(xpath = "//a[@track-name='open estimate summary']")
    private WebElement openSummaryLink;

    @FindBy(className = "close")
    private WebElement closeHintIcon;

    @FindBy(xpath = "//div[text()='Service cost updated']")
    private WebElement costUpdateMessage;

    public CloudCalculatorPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public CloudCalculatorPage addComputeEngineToEstimate() {
        WaitHelper.waitForElementVisibility(driver, addToEstimateButton);
        addToEstimateButton.click();
        WaitHelper.waitForElementVisibility(driver, computeEngineButton);
        computeEngineButton.click();
        return this;
    }

    public CloudCalculatorPage enterInstancesNumber(String number) {
        WaitHelper.waitForElementVisibility(driver, instancesNumberField);
        instancesNumberField.clear();
        instancesNumberField.sendKeys(number);
        return this;
    }

    public CloudCalculatorPage chooseOS() {
        osDropDown.click();
        WaitHelper.waitForElementVisibility(driver, osOption);
        osOption.click();
        return this;
    }

    public CloudCalculatorPage chooseProvisioningModel() {
        regularProvisioningModelButton.click();
        return this;
    }


    public CloudCalculatorPage chooseSeries() {
        seriesDropDown.click();
        WaitHelper.waitForElementVisibility(driver, seriesOption);
        seriesOption.click();
        return this;
    }

    public CloudCalculatorPage chooseMachineType() {
        machineTypeDropDown.click();
        WaitHelper.waitForElementVisibility(driver, machineTypeOption);
        machineTypeOption.click();
        return this;
    }

    public CloudCalculatorPage addGpu() {
        addGpuButton.click();
        WaitHelper.waitForElementVisibility(driver, gpuModelDropDown);
        return this;
    }

    public CloudCalculatorPage chooseGpuModel() {
        gpuModelDropDown.click();
        WaitHelper.waitForElementVisibility(driver, gpuModelOption);
        gpuModelOption.click();
        return this;
    }

    public CloudCalculatorPage chooseGpuNumber() {
        gpuNumberDropDown.click();
        WaitHelper.waitForElementVisibility(driver, gpuNumberOption);
        gpuNumberOption.click();
        return this;
    }

    public CloudCalculatorPage chooseLocalSSD() {
        localSsdDropDown.click();
        WaitHelper.waitForElementVisibility(driver, localSsdOption);
        localSsdOption.click();
        return this;
    }

    public CloudCalculatorPage chooseRegion() {
        regionDropDown.click();
        regionOption.click();
        return this;
    }

    public CloudCalculatorPage chooseCommittedUsage() {
        committedUsageButton.click();
        return this;
    }

    public CloudCalculatorPage openEstimateSummary() {
        if (closeHintIcon.isDisplayed()) closeHintIcon.click();
        WaitHelper.waitForElementVisibility(driver, costUpdateMessage);
        shareEstimateButton.click();
        WaitHelper.waitForElementVisibility(driver, openSummaryLink);
        openSummaryLink.click();
        return this;
    }
}
