package com.epam.training.bohdan_baraniuk.task3.pages;

import com.epam.training.bohdan_baraniuk.task3.utils.WaitHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CloudGooglePage {
    private static final String HOMEPAGE_URL = "https://cloud.google.com/";
    private final WebDriver driver;

    @FindBy(xpath = "//div[@class='YSM5S']")
    private WebElement searchIcon;

    @FindBy(id = "i4")
    private WebElement searchField;

    @FindBy(xpath = "//i[@aria-label='Search']")
    private WebElement searchButton;

    @FindBy(linkText = "Google Cloud Pricing Calculator")
    private WebElement calculatorLink;

    public CloudGooglePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public CloudGooglePage openPage() {
        driver.get(HOMEPAGE_URL);
        return this;
    }

    public CloudGooglePage searchByText(String text) {
        WaitHelper.waitForElementVisibility(driver, searchIcon);
        searchIcon.click();
        WaitHelper.waitForElementVisibility(driver, searchField);
        searchField.sendKeys(text);
        searchButton.click();
        return this;
    }

    public CloudGooglePage goToCalculator() {
        WaitHelper.waitForElementVisibility(driver, calculatorLink);
        calculatorLink.click();
        return this;
    }
}
