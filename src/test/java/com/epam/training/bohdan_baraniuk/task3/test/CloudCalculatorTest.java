package com.epam.training.bohdan_baraniuk.task3.test;

import com.epam.training.bohdan_baraniuk.task3.model.ComputeEngine;
import com.epam.training.bohdan_baraniuk.task3.pages.CloudCalculatorPage;
import com.epam.training.bohdan_baraniuk.task3.pages.CloudGooglePage;
import com.epam.training.bohdan_baraniuk.task3.pages.EstimateSummaryPage;
import com.epam.training.bohdan_baraniuk.task3.service.ComputeEngineCreator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CloudCalculatorTest extends CommonConditions {

    @Test
    public void summaryIsMatchedToFilledData() {
        ComputeEngine expectedComputeEngine = ComputeEngineCreator.createComputeEngineFromProperty();

        CloudGooglePage cloudGooglePage = new CloudGooglePage(driver);
        cloudGooglePage.openPage()
                .searchByText("Google Cloud Platform Pricing Calculator")
                .goToCalculator();
        CloudCalculatorPage cloudCalculatorPage = new CloudCalculatorPage(driver);
        cloudCalculatorPage.addComputeEngineToEstimate()
                .enterInstancesNumber(String.valueOf(expectedComputeEngine.getInstancesNumber()))
                .chooseOS()
                .chooseProvisioningModel()
                .chooseSeries()
                .chooseMachineType()
                .addGpu()
                .chooseGpuModel()
                .chooseGpuNumber()
                .chooseRegion()
                .chooseLocalSSD()
                .chooseCommittedUsage()
                .openEstimateSummary();

        String originalWindow = driver.getWindowHandle();
        for (String windowHandle : driver.getWindowHandles()) {
            if (!originalWindow.contentEquals(windowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }

        EstimateSummaryPage estimateSummaryPage = new EstimateSummaryPage(driver);

        ComputeEngine actualComputeEngine = new ComputeEngine(estimateSummaryPage.getNumberOfInstances(),
                estimateSummaryPage.getOS(), estimateSummaryPage.getProvisioningModel(),
                estimateSummaryPage.getMachineType().substring(0, estimateSummaryPage.getMachineType().indexOf(",")),
                estimateSummaryPage.getGpuModel(), estimateSummaryPage.getNumberOfGpus(),
                estimateSummaryPage.getLocalSSD(), estimateSummaryPage.getRegion(), estimateSummaryPage.getCommittedUsage());

        assertEquals(expectedComputeEngine, actualComputeEngine);
    }

}
