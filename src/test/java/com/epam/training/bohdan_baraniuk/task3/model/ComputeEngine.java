package com.epam.training.bohdan_baraniuk.task3.model;

import java.util.Objects;

public class ComputeEngine {
    private String instancesNumber;
    private String operatingSystem;
    private String provisioningModel;
    private String machineType;
    private String gpuModel;
    private String gpusNumber;
    private String localSSD;
    private String region;
    private String committedUsage;

    public ComputeEngine(String instancesNumber, String operatingSystem, String provisioningModel, String machineType,
                         String gpuModel, String gpusNumber, String localSSD, String region, String committedUsage) {
        this.instancesNumber = instancesNumber;
        this.operatingSystem = operatingSystem;
        this.provisioningModel = provisioningModel;
        this.machineType = machineType;
        this.gpuModel = gpuModel;
        this.gpusNumber = gpusNumber;
        this.localSSD = localSSD;
        this.region = region;
        this.committedUsage = committedUsage;
    }

    public String getInstancesNumber() {
        return instancesNumber;
    }

    public void setInstancesNumber(String instancesNumber) {
        this.instancesNumber = instancesNumber;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    public String getProvisioningModel() {
        return provisioningModel;
    }

    public void setProvisioningModel(String provisioningModel) {
        this.provisioningModel = provisioningModel;
    }

    public String getMachineType() {
        return machineType;
    }

    public void setMachineType(String machineType) {
        this.machineType = machineType;
    }

    public String getGpuModel() {
        return gpuModel;
    }

    public void setGpuModel(String gpuModel) {
        this.gpuModel = gpuModel;
    }

    public String getGpusNumber() {
        return gpusNumber;
    }

    public void setGpusNumber(String gpusNumber) {
        this.gpusNumber = gpusNumber;
    }

    public String getLocalSSD() {
        return localSSD;
    }

    public void setLocalSSD(String localSSD) {
        this.localSSD = localSSD;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCommittedUsage() {
        return committedUsage;
    }

    public void setCommittedUsage(String committedUsage) {
        this.committedUsage = committedUsage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComputeEngine that = (ComputeEngine) o;
        return Objects.equals(instancesNumber, that.instancesNumber) &&
                Objects.equals(gpusNumber, that.gpusNumber) &&
                Objects.equals(operatingSystem, that.operatingSystem) &&
                Objects.equals(provisioningModel, that.provisioningModel) &&
                Objects.equals(machineType, that.machineType) &&
                Objects.equals(gpuModel, that.gpuModel) &&
                Objects.equals(localSSD, that.localSSD) &&
                Objects.equals(region, that.region) &&
                Objects.equals(committedUsage, that.committedUsage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(instancesNumber, operatingSystem, provisioningModel, machineType, gpuModel, gpusNumber,
                localSSD, region, committedUsage);
    }

    @Override
    public String toString() {
        return "ComputeEngine{" +
                "instancesNumber=" + instancesNumber +
                ", operatingSystem='" + operatingSystem + '\'' +
                ", provisioningModel='" + provisioningModel + '\'' +
                ", machineType='" + machineType + '\'' +
                ", gpuModel='" + gpuModel + '\'' +
                ", gpusNumber=" + gpusNumber +
                ", localSSD='" + localSSD + '\'' +
                ", region='" + region + '\'' +
                ", committedUsage='" + committedUsage + '\'' +
                '}';
    }
}
