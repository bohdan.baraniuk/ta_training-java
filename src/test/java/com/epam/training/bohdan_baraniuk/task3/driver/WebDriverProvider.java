package com.epam.training.bohdan_baraniuk.task3.driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;

public class WebDriverProvider {
    private static WebDriver driver;

    private WebDriverProvider() {
    }

    public static WebDriver getDriver() {
        if (null == driver) {
            switch (System.getProperty("browser")) {
                case "firefox": {
                    driver = WebDriverManager.firefoxdriver().create();
                    break;
                }
                case "edge": {
                    driver = WebDriverManager.edgedriver().create();
                    break;
                }
                default: {
                    driver = WebDriverManager.chromedriver().create();
                }
            }
            driver.manage().window().maximize();
        }
        return driver;
    }

    public static void closeDriver() {
        driver.quit();
        driver = null;
    }
}
