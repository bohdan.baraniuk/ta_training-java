package com.epam.training.bohdan_baraniuk.task3.test;

import com.epam.training.bohdan_baraniuk.task3.driver.WebDriverProvider;
import com.epam.training.bohdan_baraniuk.task3.utils.ScreenshotTaker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;

@ExtendWith(ScreenshotTaker.class)
public class CommonConditions {
    protected WebDriver driver;


    @BeforeEach()
    public void setUp() {
        driver = WebDriverProvider.getDriver();
    }

    @AfterEach
    public void stopBrowser() {
        WebDriverProvider.closeDriver();
    }
}
