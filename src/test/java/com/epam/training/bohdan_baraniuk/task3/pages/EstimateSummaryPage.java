package com.epam.training.bohdan_baraniuk.task3.pages;

import com.epam.training.bohdan_baraniuk.task3.utils.WaitHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EstimateSummaryPage {
    private final WebDriver driver;

    @FindBy(xpath = "//span[text()='Number of Instances']/following-sibling::span")
    private WebElement numberOfInstances;

    @FindBy(xpath = "//span[text()='Operating System / Software']/following-sibling::span")
    private WebElement os;

    @FindBy(xpath = "//span[text()='Provisioning Model']/following-sibling::span")
    private WebElement provisioningModel;

    @FindBy(xpath = "//span[text()='Machine type']/following-sibling::span")
    private WebElement machineType;

    @FindBy(xpath = "//span[text()='GPU Model']/following-sibling::span")
    private WebElement gpuModel;

    @FindBy(xpath = "//span[text()='Number of GPUs']/following-sibling::span")
    private WebElement numberOfGpus;

    @FindBy(xpath = "//span[text()='Local SSD']/following-sibling::span")
    private WebElement localSSD;

    @FindBy(xpath = "//span[text()='Region']/following-sibling::span")
    private WebElement region;

    @FindBy(xpath = "//span[text()='Committed use discount options']/following-sibling::span")
    private WebElement committedUsage;

    public EstimateSummaryPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getNumberOfInstances() {
        WaitHelper.waitForElementVisibility(driver, numberOfInstances);
        return numberOfInstances.getText();
    }

    public String getOS() {
        return os.getText();
    }

    public String getProvisioningModel() {
        return provisioningModel.getText();
    }

    public String getMachineType() {
        return machineType.getText();
    }

    public String getGpuModel() {
        return gpuModel.getText();
    }

    public String getNumberOfGpus() {
        return numberOfGpus.getText();
    }

    public String getLocalSSD() {
        return localSSD.getText();
    }

    public String getRegion() {
        return region.getText();
    }

    public String getCommittedUsage() {
        return committedUsage.getText();
    }
}
