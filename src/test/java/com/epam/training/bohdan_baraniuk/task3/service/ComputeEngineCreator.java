package com.epam.training.bohdan_baraniuk.task3.service;

import com.epam.training.bohdan_baraniuk.task3.model.ComputeEngine;

public class ComputeEngineCreator {
    public static final String TESTDATA_INSTANCES_NUMBER = "testdata.ce.instancesNumber";
    public static final String TESTDATA_OPERATING_SYSTEM = "testdata.ce.operatingSystem";
    public static final String TESTDATA_PROVISIONING_MODEL = "testdata.ce.provisioningModel";
    public static final String TESTDATA_MACHINE_TYPE = "testdata.ce.machineType";
    public static final String TESTDATA_GPU_MODEL = "testdata.ce.gpuModel";
    public static final String TESTDATA_GPUS_NUMBER = "testdata.ce.gpusNumber";
    public static final String TESTDATA_LOCAL_SSD = "testdata.ce.localSSD";
    public static final String TESTDATA_REGION = "testdata.ce.region";
    public static final String TESTDATA_COMMITTED_USAGE = "testdata.ce.committedUsage";


    public static ComputeEngine createComputeEngineFromProperty(){
        return new ComputeEngine(TestDataReader.getTestData(TESTDATA_INSTANCES_NUMBER),
                TestDataReader.getTestData(TESTDATA_OPERATING_SYSTEM),
                TestDataReader.getTestData(TESTDATA_PROVISIONING_MODEL),
                TestDataReader.getTestData(TESTDATA_MACHINE_TYPE),
                TestDataReader.getTestData(TESTDATA_GPU_MODEL),
                TestDataReader.getTestData(TESTDATA_GPUS_NUMBER),
                TestDataReader.getTestData(TESTDATA_LOCAL_SSD),
                TestDataReader.getTestData(TESTDATA_REGION),
                TestDataReader.getTestData(TESTDATA_COMMITTED_USAGE));
    }
}
