package com.epam.training.bohdan_baraniuk.task1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class Task1 {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();

        driver.get("https://pastebin.com/");

        new WebDriverWait(driver, Duration.of(10, ChronoUnit.SECONDS)).until(ExpectedConditions.presenceOfElementLocated(By.id("postform-text")));
        WebElement searchInput = driver.findElement(By.id("postform-text"));
        searchInput.sendKeys("Hello from WebDriver");

        Thread.sleep(1000);
        WebElement expirationDropDown = driver.findElement(By.id("select2-postform-expiration-container"));
        expirationDropDown.click();

        new WebDriverWait(driver, Duration.of(10, ChronoUnit.SECONDS)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//li[text()='10 Minutes']")));
        WebElement expirationOption = driver.findElement(By.xpath("//li[text()='10 Minutes']"));
        expirationOption.click();

        Thread.sleep(1000);
        searchInput = driver.findElement(By.id("postform-name"));
        searchInput.sendKeys("helloweb");

        WebElement button = driver.findElement(By.xpath("//button[@type='submit']"));
        Thread.sleep(1000);
        button.click();

        Thread.sleep(10000);
        driver.quit();
    }
}

