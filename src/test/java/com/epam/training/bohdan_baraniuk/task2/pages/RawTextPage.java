package com.epam.training.bohdan_baraniuk.task2.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RawTextPage {

    @FindBy(tagName = "pre")
    private WebElement rawText;

    public RawTextPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public String getRawText() {
        return rawText.getText();
    }

}
