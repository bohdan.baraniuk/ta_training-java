package com.epam.training.bohdan_baraniuk.task2.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class PasteBinPage {
    private static final String HOMEPAGE_URL = "https://pastebin.com/";
    private final WebDriver driver;

    @FindBy(id = "postform-text")
    private WebElement paste;

    @FindBy(id = "postform-name")
    private WebElement name;

    @FindBy(id = "select2-postform-format-container")
    private WebElement syntax;

    @FindBy(xpath = "//li[text()='Bash']")
    private WebElement syntaxOption;

    @FindBy(id = "select2-postform-expiration-container")
    private WebElement expiration;

    @FindBy(xpath = "//li[text()='10 Minutes']")
    private WebElement expirationOption;

    @FindBy(xpath = "//button[text()='Create New Paste']")
    private WebElement submitButton;

    @FindBy(xpath = "//a[text()='Login']")
    private WebElement loginButton;

    public PasteBinPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void openPage() {
        driver.get(HOMEPAGE_URL);
        new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.visibilityOf(paste));
    }

    public PasteBinPage enterPaste(String pasteStr) {
        this.paste.sendKeys(pasteStr);
        return this;
    }

    public PasteBinPage enterName(String nameStr) {
        this.name.sendKeys(nameStr);
        return this;
    }

    public PasteBinPage chooseSyntax() {
        this.syntax.click();
        this.syntaxOption.click();
        return this;
    }

    public PasteBinPage chooseExpiration() {
        this.expiration.click();
        this.expirationOption.click();
        return this;
    }

    public void submit() {
        this.submitButton.click();
    }

    public void login() {
        this.loginButton.click();
    }
}