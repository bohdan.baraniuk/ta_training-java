package com.epam.training.bohdan_baraniuk.task2.pages;

import com.epam.training.bohdan_baraniuk.task2.model.User;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class LoginPage {
    private final WebDriver driver;

    @FindBy(id = "loginform-username")
    private WebElement username;

    @FindBy(id = "loginform-password")
    private WebElement password;

    @FindBy(xpath = "//button[text()='Login']")
    private WebElement loginButton;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void login(User user) {
        new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.visibilityOf(this.username));
        this.username.sendKeys(user.getUsername());
        this.password.sendKeys(user.getPassword());
        loginButton.click();
    }

}
