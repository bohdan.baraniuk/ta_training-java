package com.epam.training.bohdan_baraniuk.task2.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class PasteBinCreatedPage {
    private final WebDriver driver;

    @FindBy(xpath = "//h1")
    private WebElement name;

    @FindBy(xpath = "//a[@class='btn -small h_800']")
    private WebElement syntax;

    @FindBy(linkText = "raw")
    private WebElement rawTextLink;

    public PasteBinCreatedPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getName() {
        new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.visibilityOf(name));
        return name.getText();
    }

    public String getSyntax() {
        new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.visibilityOf(syntax));
        return syntax.getText();
    }

    public void goToRawTextPage() {
        rawTextLink.click();
    }

}
