package com.epam.training.bohdan_baraniuk.task2;

import com.epam.training.bohdan_baraniuk.task2.model.User;
import com.epam.training.bohdan_baraniuk.task2.pages.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PasteBinPageTest {
    private WebDriver driver;

    @BeforeEach
    public void browserSetup() {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void NameFieldIsFilled() {
        String expectedPaste = "git config --global user.name \"New Sheriff in Town\"\n" +
                "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
                "git push origin master --force";
        String expectedName = "how to gain dominance among developers";
        String expectedSyntax = "Bash";

        PasteBinPage filledPage = new PasteBinPage(driver);
        filledPage.openPage();

        //Next 4 lines is for singing in when the human check is triggered during creating new "paste"
//        filledPage.login();
//        LoginPage loginPage = new LoginPage(driver);
//        loginPage.login(new User("seleniumTestUser", "password1234"));
//        new ProfilePage(driver).goToPasteBinPage();

        filledPage = new PasteBinPage(driver);
        filledPage
                .enterPaste(expectedPaste)
                .chooseSyntax()
                .chooseExpiration()
                .enterName(expectedName)
                .submit();

        PasteBinCreatedPage createdPage = new PasteBinCreatedPage(driver);

        Assertions.assertEquals(expectedName, createdPage.getName());
        Assertions.assertEquals(expectedSyntax, createdPage.getSyntax());

        createdPage.goToRawTextPage();
        RawTextPage rawTextPage = new RawTextPage(driver);

        Assertions.assertEquals(expectedPaste, rawTextPage.getRawText());
    }

    @AfterEach
    public void browserTearDown() {
        driver.quit();
        driver = null;
    }

}
